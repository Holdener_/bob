//création de l'instance discord et du client
const Discord = require("discord.js");
const Client = new Discord.Client;

//authentification du bot
Client.login("empty");

//commandes
const prefix = ">";

Client.on("ready", () => {

    console.log("bot ready!");
    Client.user.setActivity("Overwatch 2", { type: 'PLAYING' });
}
);

//detecte si un message est envoyé (dans un channel ou en mp)
Client.on("message", message => {

    console.log("message detecté");
    //divise le message en plusieurs parties séparé par un espace
    var msgsplit =  message.content.split(' ');
    
    //si message d'un bot, ne fait rien
    if (message.author.bot)
        return;
    
    if (message.content.toLowerCase().includes(" bob ")
    || message.content.toLowerCase().startsWith("bob ")
    || message.content.toLowerCase().endsWith(" bob")
    || message.content.toLowerCase() == "bob")
        message.channel.send("https://64.media.tumblr.com/c11a64d79d9131696eb8c39d3843d32c/tumblr_phl6a14zSg1uujr08_400.gif");

    //message avec le prefix
    if (message.content.startsWith(prefix)){

        //bob
        if (msgsplit[0] == prefix+"bob")
            message.channel.send("https://66.media.tumblr.com/b7c81537e75a9296e7d0d675f5f0490c/tumblr_phl1u8PsA71ryhgbdo1_500.gif");


        //d ou nd

        //nb d
        var j = msgsplit[0].substring(msgsplit[0].lastIndexOf(prefix)+1,msgsplit[0].indexOf("d"));

        if (msgsplit[0].startsWith(prefix) && msgsplit[0].endsWith("d") && !isNaN(j)  && !isNaN(msgsplit[1])){

            //'n'd : prefix 'n'd 'nbfaces'
            if ( j > 1 && msgsplit[1] > 1 ){
  
                result = "Lancé de "+ j +" dés "+msgsplit[1] +" :\n";
                total = 0;
                for (i = 1; i<= j ; i++){
                    r = Math.floor(Math.random() * msgsplit[1] + 1)
                    total += r;
                    result += "dé "+ i + " : " +r+"\n";
                }
                result += "Total: "+ total ;
                message.channel.send(result);
                
            }
            //d ou 1d: prefix+d 'nbfaces'
            else if ((j == 0 || j == 1 )&& msgsplit[1] > 1){
                
                message.channel.send("Lancé d'un dé "+ msgsplit[1]+":\n"+Math.floor(Math.random() * msgsplit[1] + 1));

            }else message.channel.send("Veuillez entrer un nombre de faces valide");
   

        }

        //help
        if (msgsplit[0] == prefix+"help" || msgsplit[0] == prefix+"h"){
            message.channel.send("Bob à la rescousse!\n\n"
            +"Voici les différentes commandes:\n"
            +"lancer de dé: `"+prefix+"d 'nb faces'`\n"
            +"bob armé: `"+prefix+"bob`\n"
            +"ou alors dites seulement son nom et il réagira!"
        
            );

        }


    }
    
});




